﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DialogManager.Dialogs;

namespace DialogManagerTest.ViewModels
{
    /// <summary>
    /// View model for the testing app
    /// </summary>
    public interface IViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// The currently selected dialog
        /// </summary>
        IDialog CurrentDialog { get; set; }
        /// <summary>
        /// Get the currently selected response
        /// </summary>
        IDialog CurrentlySelectedResponse { get; set; }

        IEnumerable<IDialog> Responses { get; } 
    }
}
