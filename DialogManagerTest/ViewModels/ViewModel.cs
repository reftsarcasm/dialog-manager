﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using DialogManager.Dialogs;
using DialogManagerTest.Annotations;

namespace DialogManagerTest.ViewModels
{
    class ViewModel : IViewModel
    {
        private IDialog _currentDialog;
        private IDialog _currentlySelectedResponse;
        public event PropertyChangedEventHandler PropertyChanged;

        public IDialog CurrentDialog
        {
            get { return _currentDialog; }
            set
            {
                _currentDialog = value;
                _currentDialog.Callback.RunCallback();
                OnPropertyChanged("CurrentDialog");
                OnPropertyChanged("Responses");
            }
        }

        public IDialog CurrentlySelectedResponse
        {
            get { return _currentlySelectedResponse; }
            set
            {
                _currentlySelectedResponse = value;
                OnPropertyChanged("CurrentlySelectedResponse");
                
            }
        }

        public IEnumerable<IDialog> Responses { get { return _currentDialog.Responses.Where(x => x.ShowResponse.Show); } }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
