﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using DialogManager.DialogRepo;
using DialogManager.Dialogs;
using DialogManager.Util.Conditions.Parsers;
using DialogManager.Util.Conditions.ScriptingConditions;
using DialogManager.Util.Scripting.PythonEngine;
using DialogManager.Util.SelectCallbacks.ScriptingCallbacks;
using DialogManagerTest.ViewModels;

namespace DialogManagerTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly string _repoPath = Path.Combine(Environment.CurrentDirectory, "Resources", "TestDialogRepo.xml");
        public IViewModel ViewModel { get; protected set; }
        public MainWindow()
        {
            ViewModel = new ViewModel();
            var engine = new PythonDialogScriptingEngine();
            var conditionParser = new ScriptingConditionParser(engine, new BasicScriptingConditionFactory());
            var repo = new CachingXmlRepo(_repoPath, conditionParser, new ScriptedCallbackParser(engine));
            var topDialog = repo.GetById("TopDialog");
            ViewModel.CurrentDialog = topDialog;
            InitializeComponent();
        }
        
        private void OnProceed(object sender, SelectionChangedEventArgs e)
        {
            if (ViewModel.CurrentlySelectedResponse == null) return;
            ViewModel.CurrentDialog = ViewModel.CurrentlySelectedResponse;
            ViewModel.CurrentlySelectedResponse = null;
        }
    }
}
