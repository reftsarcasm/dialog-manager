#Dialog Manager
##Overview
This project hopes to help people to manage their dialog in an easy way, as well as encouraging scripting as an alternative for NPC behaviour modifiers.

##Building
Building this solution will require Visual Studio 2012 or later

##Example
This is a small example to get a dialog repo up and going:
```csharp
var engine = new PythonDialogScriptingEngine();
var conditionParser = new ScriptingConditionParser(engine, new BasicScriptingConditionFactory());
var repo = new CachingXmlRepo(_repoPath, conditionParser, new ScriptedCallbackParser(engine));
```

From there, grabbing out a dialog option is relatively easy:
```csharp
var topDialog = repo.GetById("TopDialog");
```

This will also load a list of responses for the dialog

Have a look at the included example application to see how to integrate it with another application