﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DialogManager.DialogRepo;
using DialogManager.Dialogs;
using DialogManager.Util.Exceptions;
using Moq;
using NUnit.Framework;

namespace DialogManagerTests.RepoTests
{
    [TestFixture]
    
    public class HardCodedRepoTests
    {
        [Test]
        public void GetByIdTest()
        {
            var mockId1 = new Mock<IDialog>();
            var id1 = 1.ToString();
            mockId1.Setup(x => x.Id)
                .Returns(id1);
            var mockId2 = new Mock<IDialog>();
            var id2 = 2.ToString();
            mockId2.Setup(x => x.Id)
                .Returns(id2);
            var impl = new HardCodedDialogRepo(new[] { mockId1.Object, mockId2.Object });
            Assert.AreSame(mockId1.Object, impl.GetById(id1));
            Assert.AreSame(mockId2.Object, impl.GetById(id2));
        }

        [Test, ExpectedException(typeof(NoSuchDialogIdException))]
        public void GetNonExistentId()
        {
            var impl = new HardCodedDialogRepo(new IDialog[] { });
            impl.GetById(1.ToString());
        }

        [Test, ExpectedException(typeof(ArgumentException))]
        public void IdExceptionPassedUp()
        {
            var mock = new Mock<IDialog>();
            mock.Setup(x => x.Id)
                .Throws(new ArgumentException("Waaaagh!"));
            var impl = new HardCodedDialogRepo(new[] { mock.Object });
            impl.GetById(string.Empty);
        }
    }
}
