﻿using System.IO;
using System.Linq;
using DialogManager.DialogRepo;
using DialogManager.Util.Conditions.Parsers;
using DialogManager.Util.Exceptions;
using DialogManager.Util.SelectCallbacks;
using Moq;
using NUnit.Framework;

namespace DialogManagerTests.RepoTests
{
    [TestFixture]
    class XmlRepoTest
    {
        private readonly string _repoPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Resources", "TestDialogRepo.xml");
        [Test]
        public void TestGet()
        {
            var repo = GetRepo();
            const string topdialogId = "TopDialog";
            const string topdialogContent = "Good sir, you look weary! Would you like to rest at my inn?";
            var topDialog = repo.GetById(topdialogId);
            Assert.AreEqual(topdialogId,topDialog.Id);
            Assert.AreEqual(topdialogContent, topDialog.Content);
            Assert.IsTrue(topDialog.ShowResponse.Show);
            Assert.AreEqual(2, topDialog.Responses.Count());
            Assert.IsTrue(topDialog.Responses.Any(x => x.Id == "YesDialog"));
            Assert.IsTrue(topDialog.Responses.Any(x => x.Id == "NoDialog"));

            //If this one loads ok, then chances are the others have too
            var yesDialog = topDialog.Responses.First(x => x.Id == "YesDialog");
            Assert.AreEqual("Excellent! Follow me to your room", yesDialog.Content);
            Assert.AreEqual(0,yesDialog.Responses.Count());
            Assert.IsFalse(yesDialog.ShowResponse.Show);
        }

        private CachingXmlRepo GetRepo()
        {
            var parser = new BooleanConditionParser();
            var callbackParser = new Mock<ISelectedCallbackParser>();
            var repo = new CachingXmlRepo(_repoPath, parser, callbackParser.Object);
            return repo;
        }

        [Test, ExpectedException(typeof(NoSuchDialogIdException))]
        public void TestNoSuchId()
        {
            var repo = GetRepo();
            const string topdialogId = "WAAAAGH!";
            repo.GetById(topdialogId);
        }
    }
}
