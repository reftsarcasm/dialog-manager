﻿using DialogManager.Util.Conditions.Parsers;
using DialogManager.Util.Conditions.ScriptingConditions;
using DialogManager.Util.Scripting;
using Moq;
using NUnit.Framework;

namespace DialogManagerTests.ConditionTests.Parsers
{
    /// <summary>
    /// Tests for script engine delegator
    /// </summary>
    /// <remarks>
    /// The script engine parser should delegate 100% to the engine behind it
    /// </remarks>
    [TestFixture]
    public class ScriptingEngineParserTests
    {
        [Test]
        public void FlowNoInitTest()
        {
            const string exprString = "test string this isn't an actual expression";
            var scopeMock = new Mock<IDialogScriptScope>();
            scopeMock.Setup(x => x.RunInScope(It.IsAny<string>()))
                .Verifiable();
            var scriptEngineMock = new Mock<IDialogScriptingEngine>();
            scriptEngineMock.Setup(x => x.CreateScope())
                .Returns(scopeMock.Object);
            var factoryMock = new Mock<IScriptingConditionFactory>();
            factoryMock.Setup(x => x.Create(It.Is<IDialogScriptScope>(y => y == scopeMock.Object), It.Is<string>(y => y == exprString)))
                .Returns((IScriptingCondition)null)
                .Verifiable();

            var parser = new ScriptingConditionParser(scriptEngineMock.Object, factoryMock.Object);
            parser.Parse(exprString);
            scopeMock.Verify(x => x.RunInScope(It.IsAny<string>()), Times.Never, "Init script was called when it hadn't been set!");
            factoryMock.Verify();
        }

        [Test]
        public void FlowWithInitTest()
        {
            const string exprString = "test string this isn't an actual expression";
            const string initScript = "init script";
            var scopeMock = new Mock<IDialogScriptScope>();
            scopeMock.Setup(x => x.RunInScope(It.Is<string>(y => y == exprString)))
                .Verifiable();
            scopeMock.Setup(x => x.RunInScope(It.Is<string>(y => y == initScript)))
                .Verifiable();
            var scriptEngineMock = new Mock<IDialogScriptingEngine>();
            scriptEngineMock.Setup(x => x.CreateScope())
                .Returns(scopeMock.Object);
            var factoryMock = new Mock<IScriptingConditionFactory>();
            factoryMock.Setup(x => x.Create(It.Is<IDialogScriptScope>(y => y == scopeMock.Object), It.Is<string>(y => y == exprString)))
                .Returns((IScriptingCondition)null)
                .Verifiable();

            var parser = new ScriptingConditionParser(scriptEngineMock.Object, factoryMock.Object);
            parser.SetInitScript(initScript);
            parser.Parse(exprString);
            scopeMock.Verify(x => x.RunInScope(exprString), Times.Exactly(0));
            scopeMock.Verify(x => x.RunInScope(initScript), Times.Exactly(1));
            factoryMock.Verify();
        }
    }
}
