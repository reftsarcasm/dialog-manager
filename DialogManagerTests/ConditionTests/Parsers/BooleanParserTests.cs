﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DialogManager.Util.Conditions.Parsers;
using NUnit.Framework;

namespace DialogManagerTests.ConditionTests.Parsers
{
    [TestFixture]
    public class BooleanParserTests
    {
        [Test]
        public void TrueCondition()
        {
            var parser = new BooleanConditionParser();
            var condition = parser.Parse("true");
            Assert.IsTrue(condition.Show);
        }

        [Test]
        public void AllCapsTrueCondition()
        {
            var parser = new BooleanConditionParser();
            var condition = parser.Parse("TRUE");
            Assert.IsTrue(condition.Show);
        }

        [Test]
        public void FalseCondition()
        {
            var parser = new BooleanConditionParser();
            var condition = parser.Parse("false");
            Assert.IsFalse(condition.Show);
        }

        [Test]
        public void AllCapsFalseCondition()
        {
            var parser = new BooleanConditionParser();
            var condition = parser.Parse("FALSE");
            Assert.IsFalse(condition.Show);
        }

        [Test, ExpectedException(typeof(FormatException))]
        public void GarbageInput()
        {
            var parser = new BooleanConditionParser();
            var condition = parser.Parse("This isn't a boolean");
        }
    }
}
