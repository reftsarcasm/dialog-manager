﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DialogManager.Util.Conditions.ScriptingConditions;
using DialogManager.Util.Scripting;
using Moq;
using NUnit.Framework;

namespace DialogManagerTests.ConditionTests.Factories
{
    [TestFixture]
    public class ScriptingConditionFactoryTests
    {
        [Test]
        public void CreateTest()
        {
            var scopeMock = new Mock<IDialogScriptScope>();
            const string fakeScript = "fake string";
            var factory = new BasicScriptingConditionFactory();
            var ret = factory.Create(scopeMock.Object, fakeScript);
            Assert.AreEqual(typeof(ScriptingCondition), ret.GetType());
        }
    }
}
