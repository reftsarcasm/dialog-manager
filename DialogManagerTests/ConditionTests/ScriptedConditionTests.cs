﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DialogManager.Util.Conditions.ScriptingConditions;
using DialogManager.Util.Scripting;
using Moq;
using NUnit.Framework;

namespace DialogManagerTests.ConditionTests
{
    [TestFixture]
    class ScriptedConditionTests
    {
        [Test]
        public void FlowTest()
        {
            const string script = "test script";
            var scopeMock = new Mock<IDialogScriptScope>();
            scopeMock.Setup(x => x.RunInScope(It.Is<string>(y => y == script)))
                .Returns(true)
                .Verifiable();

            var condition = new ScriptingCondition(scopeMock.Object, script);
            var result = condition.Show;
            scopeMock.Verify();
            Assert.IsTrue(result);
        }
    }
}
