﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DialogManager.DialogRepo;
using DialogManager.Dialogs;
using Moq;
using NUnit.Framework;

namespace DialogManagerTests.DialogTests
{
    [TestFixture]
    class LazyLoaderTests
    {
        [Test]
        public void TestRepoLoading()
        {
            const string id = "test";
            var dialogMock = new Mock<IDialog>();
            var repoMock = new Mock<IDialogRepo>();
            repoMock.Setup(x => x.GetById(It.Is<string>(y => y == id)))
                .Returns(dialogMock.Object)
                .Verifiable();
            var dialog = new LazyLoadingDialog(repoMock.Object, id);
            //Should lazy load
            var resp = dialog.Content;
            repoMock.Verify();
        }
    }
}
