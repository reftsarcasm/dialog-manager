﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DialogManager.Util.Scripting.PythonEngine;
using Microsoft.Scripting;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace DialogManagerTests.Scripting
{
    [TestFixture]
    class PythonEngineTests
    {
        [Test]
        public void GlobalSetStringTests()
        {
            var engine = new PythonDialogScriptingEngine();
            const string testString = "Testing this stuff";
            const string testName = "test";
            engine.SetGlobal(testName, testString);
            var scope = engine.CreateScope();
            var ret = scope[testName];
            Assert.AreEqual(testString, ret);
        }

        [Test]
        public void GlobalSetObjectTest()
        {
            var l = new List<string>();
            var engine = new PythonDialogScriptingEngine();
            const string testName = "test";
            engine.SetGlobal(testName, l);
            var scope = engine.CreateScope();
            var ret = scope[testName];
            Assert.AreSame(l, ret);
        }

        [Test]
        public void TestScriptRunSucceed()
        {
            var engine = new PythonDialogScriptingEngine();
            var scope = engine.CreateScope();
            var ret = (bool)scope.RunInScope("True");
            Assert.IsTrue(ret);
            ret = (bool)scope.RunInScope("False");
            Assert.IsFalse(ret);
        }

        [Test, ExpectedException(typeof(SyntaxErrorException))]
        public void TestScriptFail()
        {
            var engine = new PythonDialogScriptingEngine();
            var scope = engine.CreateScope();
            var ret = (bool)scope.RunInScope("not valid python woot woot");
            
        }
    }
}
