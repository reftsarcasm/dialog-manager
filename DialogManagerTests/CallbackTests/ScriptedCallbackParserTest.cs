﻿using DialogManager.Util.Scripting;
using DialogManager.Util.SelectCallbacks.ScriptingCallbacks;
using Moq;
using NUnit.Framework;

namespace DialogManagerTests.CallbackTests
{
    [TestFixture]
    class ScriptedCallbackParserTest
    {
        [Test]
        public void FlowTest()
        {
            //Only thing we can really test right now
            var scopeMock = new Mock<IDialogScriptScope>();
            var engineMock = new Mock<IDialogScriptingEngine>();
            engineMock.Setup(x => x.CreateScope())
                .Returns(scopeMock.Object)
                .Verifiable();
            var parser = new ScriptedCallbackParser(engineMock.Object);

            var ret = parser.Parse("test scope");

            engineMock.Verify();
        }
    }
}
