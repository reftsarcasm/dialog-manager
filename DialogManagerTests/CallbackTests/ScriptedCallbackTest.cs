﻿using DialogManager.Util.Scripting;
using DialogManager.Util.SelectCallbacks.ScriptingCallbacks;
using Moq;
using NUnit.Framework;

namespace DialogManagerTests.CallbackTests
{
    [TestFixture]
    class ScriptedCallbackTest
    {
        [Test]
        public void FlowTest()
        {
            const string testScript = "test script";
            var scopeMock = new Mock<IDialogScriptScope>();
            scopeMock.Setup(x => x.RunInScope(testScript))
                .Verifiable();
            var callback = new ScriptedCallback(scopeMock.Object, testScript);
            callback.RunCallback();
            scopeMock.Verify();
        }
    }
}
