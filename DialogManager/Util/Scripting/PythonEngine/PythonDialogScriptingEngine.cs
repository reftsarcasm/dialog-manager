﻿using System.Collections.Generic;
using IronPython.Hosting;
using Microsoft.Scripting.Hosting;

namespace DialogManager.Util.Scripting.PythonEngine
{
    /// <summary>
    /// Implementing class for a Python scripting engine
    /// </summary>
    /// <remarks>
    /// This is kind of like a factory pattern
    /// </remarks>
    public class PythonDialogScriptingEngine : IDialogScriptingEngine
    {
        private ScriptEngine _engine;
        private Dictionary<string, object> _globals; 

        public PythonDialogScriptingEngine()
        {
            _engine = Python.CreateEngine();
            _globals = new Dictionary<string, object>();
        }
        public void SetGlobal(string name, object val)
        {
            _globals[name] = val;
        }

        public IDialogScriptScope CreateScope()
        {
            return new GenericDialogScriptingScope(_engine.CreateScope(_globals));
        }

    }
}
