﻿using System;

namespace DialogManager.Util.Scripting
{
    /// <summary>
    /// A scripting scope. Everything run in this scope will be contained to this scope and destroyed after it's done running
    /// </summary>
    public interface IDialogScriptScope : IDisposable
    {
        /// <summary>
        /// Run an expression in this scope
        /// </summary>
        /// <param name="script">Expression to run</param>
        /// <returns>The last value from the script</returns>
        object RunInScope(string script);

        /// <summary>
        /// Get or set a variable within this scope
        /// </summary>
        /// <param name="variableName">Name of the variable</param>
        object this[string variableName] { get; set; }
    }
}
