﻿namespace DialogManager.Util.Scripting
{
    /// <summary>
    /// Scripting engine interface
    /// </summary>
    public interface IDialogScriptingEngine
    {
        /// <summary>
        /// Set a global variable
        /// </summary>
        /// <param name="name">Name of the global variable</param>
        /// <param name="val">Global's value</param>
        void SetGlobal(string name, object val);

        /// <summary>
        /// Create a scripting scope for this engine with all the defined globals
        /// </summary>
        /// <returns>A new scripting scope from this engine</returns>
        IDialogScriptScope CreateScope();

    }
}
