﻿using Microsoft.Scripting.Hosting;

namespace DialogManager.Util.Scripting
{
    /// <summary>
    /// Implementing class for a generic scripting scope. Uses DLR types to 
    /// </summary>
    public class GenericDialogScriptingScope : IDialogScriptScope
    {
        private ScriptScope _scope;

        public GenericDialogScriptingScope(ScriptScope scope)
        {
            _scope = scope;
        }
        public void Dispose()
        {
            _scope = null;
        }

        public object RunInScope(string script)
        {
            var source = _scope.Engine.CreateScriptSourceFromString(script);
            return source.Execute<object>(_scope);
        }

        public object this[string variableName]
        {
            get { return _scope.GetVariable<object>(variableName); }
            set { _scope.SetVariable(variableName, value); }
        }
    }
}
