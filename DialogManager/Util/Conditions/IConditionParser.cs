﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DialogManager.Util.Conditions
{
    /// <summary>
    /// Interface to parse a condition expression
    /// </summary>
    public interface IConditionParser
    {
        /// <summary>
        /// Parse a string for a condition
        /// </summary>
        /// <param name="expr">The expression string to parse</param>
        /// <returns>A condition object</returns>
        ICondition Parse(string expr);
    }
}
