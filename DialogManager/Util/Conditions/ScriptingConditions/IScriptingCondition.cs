﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DialogManager.Util.Conditions.ScriptingConditions
{
    /// <summary>
    /// Interface for scripted conditions
    /// </summary>
    /// <remarks>Empty for now, but scripted conditions may need extra functionality</remarks>
    public interface IScriptingCondition : ICondition
    {
    }
}
