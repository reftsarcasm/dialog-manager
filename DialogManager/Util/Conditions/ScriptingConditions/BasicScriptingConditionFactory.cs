﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DialogManager.Util.Scripting;

namespace DialogManager.Util.Conditions.ScriptingConditions
{
    /// <summary>
    /// Basic factory type for scripted conditions
    /// </summary>
    /// <remarks>Only the default implementation.</remarks>
    public class BasicScriptingConditionFactory : IScriptingConditionFactory
    {
        public IScriptingCondition Create(IDialogScriptScope scope, string script)
        {
            return new ScriptingCondition(scope, script);
        }
    }
}
