﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DialogManager.Util.Scripting;

namespace DialogManager.Util.Conditions.ScriptingConditions
{
    /// <summary>
    /// Factory for scripting condition objects
    /// </summary>
    public interface IScriptingConditionFactory
    {
        /// <summary>
        /// Create a scripted condition
        /// </summary>
        /// <returns>The newly created condition</returns>
        IScriptingCondition Create(IDialogScriptScope scope, string script);
    }
}
