using DialogManager.Util.Scripting;

namespace DialogManager.Util.Conditions.ScriptingConditions
{
    /// <summary>
    /// Script backed condition
    /// </summary>
    public class ScriptingCondition : IScriptingCondition
    {
        private readonly IDialogScriptScope _scope;
        private readonly string _script;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="scope">Scripting scope to run in</param>
        /// <param name="script">Expression to run</param>
        public ScriptingCondition(IDialogScriptScope scope, string script)
        {
            _scope = scope;
            _script = script;
        }

        /// <summary>
        /// Whether to show the dialog
        /// </summary>
        /// <remarks>
        /// Will reevaluate the script everytime this is called, call sparingly for 
        /// performance reason and cache results where possible
        /// </remarks>
        public bool Show
        {
            get { return (bool) _scope.RunInScope(_script); }
        }
    }
}
