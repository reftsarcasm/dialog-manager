﻿namespace DialogManager.Util.Conditions
{
    /// <summary>
    /// Interface for conditional objects on dialogs
    /// </summary>
    public interface ICondition
    {
        /// <summary>
        /// Whether or not to show this dialog option
        /// </summary>
        bool Show { get; }
    }
}
