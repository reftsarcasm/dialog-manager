﻿using System;
using DialogManager.Util.Conditions.ScriptingConditions;
using DialogManager.Util.Scripting;

namespace DialogManager.Util.Conditions.Parsers
{
    /// <summary>
    /// Will delegate condition parsing to a scripting engine
    /// </summary>
    public class ScriptingConditionParser : IConditionParser
    {
        private readonly IDialogScriptingEngine _scriptingEngine;
        private string _initScript;
        private IScriptingConditionFactory _conditionFactory;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="scriptingEngine">Scripting engine to use</param>
        /// <param name="conditionFactory">Condition creational object</param>
        public ScriptingConditionParser(IDialogScriptingEngine scriptingEngine, IScriptingConditionFactory conditionFactory)
        {
            _scriptingEngine = scriptingEngine;
            _initScript = string.Empty;
            _conditionFactory = conditionFactory;
        }

        /// <summary>
        /// Set the script to be run when parsing an expression
        /// </summary>
        /// <param name="initScript"></param>
        public void SetInitScript(string initScript)
        {
            _initScript = initScript;
        }

        public ICondition Parse(string expr)
        {
            var scope = _scriptingEngine.CreateScope();
            //Run the initialiser script
            if(!string.IsNullOrEmpty(_initScript))
                scope.RunInScope(_initScript);
            return _conditionFactory.Create(scope, expr);
        }
    }
}
