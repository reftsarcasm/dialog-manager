﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DialogManager.Util.Conditions.SimpleConditions;

namespace DialogManager.Util.Conditions.Parsers
{
    /// <summary>
    /// Parse a boolean out of a string
    /// </summary>
    public class BooleanConditionParser : IConditionParser
    {
        public ICondition Parse(string expr)
        {
            return new BooleanCondition(bool.Parse(expr.ToLower()));
        }
    }
}
