﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DialogManager.Util.Conditions.SimpleConditions
{
    /// <summary>
    /// Boolean conditon
    /// </summary>
    public class BooleanCondition : ICondition
    {
        public BooleanCondition(bool val)
        {
            Show = val;
        }
        public bool Show { get; private set; }
    }
}
