﻿using System;

namespace DialogManager.Util.Exceptions
{
    public class DuplicateDialogIdException : Exception
    {
        public DuplicateDialogIdException(string id) : base(string.Format("Duplicate ID {0}!", id))
        {
            
        }
    }
}