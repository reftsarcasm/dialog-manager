﻿using System;

namespace DialogManager.Util.Exceptions
{
    public class MissingElementException : Exception
    {
        public MissingElementException(string elementName) 
            : base(string.Format("Required element {0} not found!",elementName)){ }
    }
}
