﻿using System;

namespace DialogManager.Util.Exceptions
{
    /// <summary>
    /// Exception class for non-existent ID queries. 
    /// </summary>
    public class NoSuchDialogIdException : Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">ID that was asked for</param>
        public NoSuchDialogIdException(string id)
            : base(string.Format("No such dialog option with ID '{0}'", id))
        {
        }
    }
}