﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DialogManager.Util.SelectCallbacks
{
    /// <summary>
    /// Interface for run a selected callback
    /// </summary>
    public interface ISelectedCallback
    {
        /// <summary>
        /// Run this callback
        /// </summary>
        void RunCallback();
    }
}
