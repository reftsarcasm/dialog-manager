﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DialogManager.Util.SelectCallbacks
{
    /// <summary>
    /// Interface for selected callback expressions
    /// </summary>
    public interface ISelectedCallbackParser
    {
        /// <summary>
        /// Parse a callback expression
        /// </summary>
        /// <param name="expression">Expression string to parse</param>
        /// <returns>The callback to call</returns>
        ISelectedCallback Parse(string expression);
    }
}
