﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DialogManager.Util.Conditions;
using DialogManager.Util.Scripting;

namespace DialogManager.Util.SelectCallbacks.ScriptingCallbacks
{
    /// <summary>
    /// Class to create scripted callbacks
    /// </summary>
    public class ScriptedCallbackParser : ISelectedCallbackParser
    {
        private readonly IDialogScriptingEngine _engine;

        public ScriptedCallbackParser(IDialogScriptingEngine engine)
        {
            _engine = engine;
        }

        public ISelectedCallback Parse(string expr)
        {
            return new ScriptedCallback(_engine.CreateScope(), expr);
        }
    }
}
