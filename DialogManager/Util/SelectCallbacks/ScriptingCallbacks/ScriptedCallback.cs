﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DialogManager.Util.Scripting;
using Microsoft.Scripting.Hosting;

namespace DialogManager.Util.SelectCallbacks.ScriptingCallbacks
{
    public class ScriptedCallback : ISelectedCallback
    {
        private readonly IDialogScriptScope _scope;
        private readonly string _callbackBody;

        public ScriptedCallback(IDialogScriptScope scope, string callback)
        {
            _scope = scope;
            _callbackBody = callback;
        }

        public void RunCallback()
        {
            _scope.RunInScope(_callbackBody);
        }
    }
}
