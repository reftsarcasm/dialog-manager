﻿using System.Collections.Generic;
using DialogManager.Util.Conditions;
using DialogManager.Util.SelectCallbacks;

namespace DialogManager.Dialogs
{
    /// <summary>
    /// Dialog interface
    /// </summary>
    public interface IDialog
    {
        /// <summary>
        /// Id of the dialog
        /// </summary>
        string Id { get; }
        /// <summary>
        /// Content of the dialog
        /// </summary>
        string Content { get; }

        /// <summary>
        /// Responses to the dialog
        /// </summary>
        /// <remarks>
        /// Empty means it's a terminal state, with nothing left to say
        /// </remarks>
        IEnumerable<IDialog> Responses { get; }
        /// <summary>
        /// The leading response to get here
        /// </summary>
        string LeadingResponse { get; }
        /// <summary>
        /// Whether or not to show this response
        /// </summary>
        ICondition ShowResponse { get; }
        /// <summary>
        /// Callback to run when this has been to selected
        /// </summary>
        ISelectedCallback Callback { get; }
    }
}
