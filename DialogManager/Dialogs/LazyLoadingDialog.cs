﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DialogManager.DialogRepo;
using DialogManager.Util.Conditions;
using DialogManager.Util.SelectCallbacks;

namespace DialogManager.Dialogs
{
    /// <summary>
    /// Lazily loading content node
    /// </summary>
    public class LazyLoadingDialog : IDialog
    {
        private IDialog _inner;
        private IDialog Inner {
            get { return _inner ?? (_inner = _dialogRepo.GetById(_id)); }
        }
        private readonly IDialogRepo _dialogRepo;
        private readonly string _id;

        public LazyLoadingDialog(IDialogRepo repo, string id)
        {
            _dialogRepo = repo;
            _id = id;
        } 

        public string Id
        {
            get { return _id; }
        }

        public string Content
        {
            get
            {
                return Inner.Content;
            }
        }

        public IEnumerable<IDialog> Responses
        {
            get { return Inner.Responses; }
        }

        public string LeadingResponse
        {
            get { return Inner.LeadingResponse; }
        }

        public ICondition ShowResponse
        {
            get { return Inner.ShowResponse; }
        }

        public ISelectedCallback Callback { get { return Inner.Callback; }}
    }
}
