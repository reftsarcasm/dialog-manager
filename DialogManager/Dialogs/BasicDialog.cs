﻿using System.Collections.Generic;
using DialogManager.Util.Conditions;
using DialogManager.Util.SelectCallbacks;

namespace DialogManager.Dialogs
{
    /// <summary>
    /// Class for a basic implementation of the dialog
    /// </summary>
    public class BasicDialog : IDialog
    {
        public string Id { get; set; }
        public string LeadingResponse { get; set; }
        public ICondition ShowResponse { get; set; }
        public ISelectedCallback Callback { get; set; }
        public string Content { get; set; }
        public IEnumerable<IDialog> Responses { get; set; }
    }
}
