﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.XPath;
using DialogManager.Dialogs;
using DialogManager.Util.Conditions;
using DialogManager.Util.Exceptions;
using DialogManager.Util.SelectCallbacks;

namespace DialogManager.DialogRepo
{
    /// <summary>
    /// XML backed repo that will cache requests as they're loaded. Should make sure that we only have one copy 
    /// of a dialog at a time
    /// </summary>
    public class CachingXmlRepo : IDialogRepo
    {
        private string _path;
        private Dictionary<string, IDialog> _cache;
        private IConditionParser _conditionParser;
        private ISelectedCallbackParser _callbackParser;

        public CachingXmlRepo(string path, IConditionParser conditionParser, ISelectedCallbackParser callbackParser)
        {
            _path = path;
            _cache = new Dictionary<string, IDialog>();
            _conditionParser = conditionParser;
            _callbackParser = callbackParser;
        }
        public IDialog GetById(string id)
        {
            if (_cache.ContainsKey(id))
                return _cache[id];
            var doc = new XPathDocument(_path);
            var nav = doc.CreateNavigator();

            var xPathExpression = string.Format("/DialogRepo/Dialog[@Id='{0}']", id);
            var dialogElement = nav.SelectSingleNode(xPathExpression);
            if (dialogElement == null)
            {
                throw new NoSuchDialogIdException(id);
            }
            
            var contentNode = dialogElement.SelectSingleNode(string.Format("/DialogRepo/Dialog[@Id='{0}']/Content", id));
            if (contentNode == null)
            {
                throw new MissingElementException("Content");
            }
            var content = contentNode.InnerXml.Trim();

            var responses = dialogElement.GetAttribute("Responses",string.Empty).Split(',')
                .Where(x => !string.IsNullOrEmpty(x))
                .Select(x => new LazyLoadingDialog(this, x.Trim()));

            var leadingResponseNode = dialogElement
                .SelectSingleNode(string.Format("/DialogRepo/Dialog[@Id='{0}']/LeadingResponse", id));
            var leadingResponse = leadingResponseNode == null
                ? string.Empty
                : leadingResponseNode.InnerXml;

            var showAttribute = dialogElement.GetAttribute("Show", "");
            var show = _conditionParser.Parse(showAttribute);

            var callbackAttribute = dialogElement.GetAttribute("Callback", "");
            var callback = _callbackParser.Parse(callbackAttribute);

            var basicDialog = new BasicDialog()
            {
                Content = content,
                Id = id,
                Responses = responses,
                LeadingResponse = leadingResponse,
                ShowResponse = show,
                Callback = callback
            };
            _cache[id] = basicDialog;
            return basicDialog;
        }
    }
}
