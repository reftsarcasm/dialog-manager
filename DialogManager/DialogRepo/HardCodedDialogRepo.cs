﻿using System;
using System.Collections.Generic;
using System.Linq;
using DialogManager.Dialogs;
using DialogManager.Util.Exceptions;

namespace DialogManager.DialogRepo
{
    public class HardCodedDialogRepo : IDialogRepo
    {
        private IEnumerable<IDialog> _dialogs; 

        /// <summary>
        /// Constructor 
        /// </summary>
        /// <param name="dialogOptions">Dialogs that we can load</param>
        public HardCodedDialogRepo(IEnumerable<IDialog> dialogOptions)
        {
            _dialogs = dialogOptions;
        }

        public IDialog GetById(string id)
        {
            try
            {
                return _dialogs.First(x => x.Id == id);
            }
            catch (InvalidOperationException)
            {
                throw new NoSuchDialogIdException(id);
            }
        }
    }
}
