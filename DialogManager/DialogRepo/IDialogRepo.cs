﻿using DialogManager.Dialogs;

namespace DialogManager.DialogRepo
{
    /// <summary>
    /// Interface for dialog queries
    /// </summary>
    public interface IDialogRepo
    {
        /// <summary>
        /// Get a dialog by ID from the source
        /// </summary>
        /// <param name="id">ID of the dialog to get</param>
        /// <returns>The associated dialog</returns>
        IDialog GetById(string id);
    }
}
